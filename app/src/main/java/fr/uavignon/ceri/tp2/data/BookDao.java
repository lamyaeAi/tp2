package fr.uavignon.ceri.tp2.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static fr.uavignon.ceri.tp2.data.Book.books;

@Dao
public interface BookDao {
    @Insert
void insertBook(Book book);

    @Query("SELECT * FROM books WHERE id = :id")
List<Book> getBook(long id);

 @Query("DELETE FROM books WHERE id = :id")
 void deleteBook(long id);

 @Update
void updateProduct(Book product);

 @Query("SELECT * FROM books")
 LiveData<List<Book>> getAllBooks();

   @Query("DELETE FROM books")
     void deleteAllBooks();
}

