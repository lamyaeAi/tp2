package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookDao;
import fr.uavignon.ceri.tp2.data.BookRoomDatabase;

public class BookRepository {
    private MutableLiveData<List<Book>> findResults =
            new MutableLiveData<>();
 private LiveData<List<Book>> allBooks;

        private BookDao bookDao;
        public BookRepository(Application application) {
         BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
         bookDao = db.BookDao();
         allBooks = ((BookDao) bookDao).getAllBooks();
         }

        public LiveData<List<Book>> getAllBooks() {
         return allBooks;
         }

         public MutableLiveData<List<Book>> getSearchResults() {
         return findResults;
        }

}
